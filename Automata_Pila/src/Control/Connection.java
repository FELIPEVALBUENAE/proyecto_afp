package Control;

import java.io.IOException;

import Model.Automata;
import View.Ventana;

public class Connection {
	
	private Automata automata; 
	private Ventana ventana; 

	/*
	 * Metodo que pasa el fichero desde la gui al modelo
	 */
	public void setPathFile(String str) {
		System.out.println(automata.getPath());
		automata.setPath(str);
		
	}
	
	public void datosIniciales() throws IOException {
		automata.ReadAutomata();
		ventana.getPanelSimulacion().getPanelDatosAu().getSetStates().setText(automata.getSetQ());
	}
	
	
}
