package Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Automata {
	
	/*
	 * ATRIBUTOS
	 */
	
	private ArrayList<String> setQ; //Conjunto finito de estados
	private ArrayList<String> setSigma; //
	private ArrayList<String> setR; //
	private ArrayList<String> setF; //
	private ArrayList<String[]> setTransitions; //
	private String initialState;
	private String initialPushdownSymbol;

	private String actualState;
	private Stack<String> stack;
	
	private String path = null;
	
	public Automata(){

		setQ = new ArrayList<String>();
		setSigma = new ArrayList<String>();
		setR = new ArrayList<String>();
		setF = new ArrayList<String>();
		setTransitions = new ArrayList<String[]>();
		stack = new Stack<String>();
		
		try {
			this.ReadAutomata();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * CAPTAR INFORMACI�N DEL AUT�MATA
	 */
	public void ReadAutomata() throws IOException  {
		// Almacenar la informacion del fichero
		String textoFichero;
		FileReader leerFichero = new FileReader(path);
		BufferedReader bufferLectura = new BufferedReader(leerFichero);
		int linea = 0;
		while ((textoFichero = bufferLectura.readLine()) != null) {
			if (textoFichero.matches("#.*"))
				continue;
			else if (textoFichero.matches("\b*"))
				continue;
			else {
				if (linea >= 6) {
					String separarEspacios[] = textoFichero.split(" ");
					setTransitions.add(separarEspacios);
				} else {
					String separarEspacios[] = textoFichero.split(" ");
					for (int i = 0; i < separarEspacios.length; i++) {
						if (separarEspacios[i].matches("#.*"))
							break;
						else {
							if (linea == 0)
								setQ.add(separarEspacios[i]);
							else if (linea == 1)
								setSigma.add(separarEspacios[i]);
							else if (linea == 2)
								setR.add(separarEspacios[i]);
							else if (linea == 3)
								initialState = separarEspacios[i];
							else if (linea == 4)
								initialPushdownSymbol = separarEspacios[i];
							else if (linea == 5)
								setF.add(separarEspacios[i]);
						}
					}
				}// END IF ELSE INTERIOR
				linea++;
			}// END IF ELSE EXTERIOR
		}// END WHILE
		bufferLectura.close();
		
	}

	/*
	 * MOSTRAR INFORMACI�N DEL AUT�MATA
	 */
	
	public void mostrarInformacionAutomata() {
		System.out.println("Informaci�n del aut�mata con pila");
		System.out.println();
		System.out.println("Estado inicial: " + initialState);
		System.out.println("Símbolo inicial de la pila: " + initialPushdownSymbol);
		System.out.println("Conjunto de estados: " + setQ.toString());
		System.out.println("Alfabeto del lenguaje: " + setSigma.toString());
		System.out.println("Alfabeto de la pila: " + setR.toString());
		System.out.println("Conjunto de estados finales: " + setF.toString());
		System.out.println("Conjunto de transiciones:");
		for (int i = 0; i < setTransitions.size(); i++)
			System.out.println(Arrays.toString(setTransitions.get(i)));
		System.out.println("");
	}
	
	/*
	 * EJECUTAR AUTOMATA
	 */
	
	public void ejecutarAutomata() {
		
		String cadenaEntrada;
		stack.clear();
		stack.push(initialPushdownSymbol);
		actualState = initialState;
		// El usuario inserta la cadena
		System.out.println("Inserte la cadena a probar:");
		System.out.println("(Introduzca siempre 'e' al final)");
		Scanner imputUsuario = new Scanner(System.in);
		cadenaEntrada = imputUsuario.nextLine();
		imputUsuario.close();
		// Empezamos a evaluar la cadena de entrada
		boolean noTransiciones = true;
		int numeroIteraciones = cadenaEntrada.length();//hacemos esto porque vamos a ir eliminando la entrada
		for (int i = 0; i < numeroIteraciones; i++) {
			
			if (noTransiciones == true) { //si existen transiciones sigue
				//vamos eliminando el elemento de la cadena tratado 
				String simboloTratado = cadenaEntrada.substring(0, 1);
				System.out.println("//------------------Simbolo Tratado "+simboloTratado+"-----------------------//");
				cadenaEntrada = cadenaEntrada.substring(1);
				noTransiciones = false;//suponemos, a priori, que no hay transiciones
				
				for (int j = 0; j < setTransitions.size(); j++) {
					//si encuentra alguna transicion entra
					System.out.println("	-----------------------"+"\n		IF Itera "+ j +"\n	-----------------------"+
							"\n		"+actualState.equals(setTransitions.get(j)[0])+" "+actualState+" == "+setTransitions.get(j)[0]+
							"\n		"+ simboloTratado.equals(setTransitions.get(j)[1])+" "+simboloTratado+" == "+setTransitions.get(j)[1]+
							"\n		"+ stack.peek().equals(setTransitions.get(j)[2])+" "+stack.peek()+" == "+setTransitions.get(j)[2]+" IF \n	----------------------- \n");
					if(simboloTratado != "e" && cadenaEntrada != "") {
						
						if (actualState.equals(setTransitions.get(j)[0]) && simboloTratado.equals(setTransitions.get(j)[1])	&& stack.peek().equals(setTransitions.get(j)[2])) {
							
							
							System.out.println(" ");
							System.out.println("----------------------------------");
							System.out.println("----------------------------------");
							System.out.println("Transici�n - "+j); 
							System.out.println("Estado Actual "+actualState);
							System.out.println("Cadena de entrada "+cadenaEntrada);
							System.out.println("Simbolo Tratado "+simboloTratado);
							System.out.println("Nuevo estado "+setTransitions.get(j)[3]);
							System.out.println("----------------------------------");
							System.out.println("----------------------------------");
							System.out.println(" ");
							
							actualState = setTransitions.get(j)[3];
							System.out.println("Estado Actual "+actualState);
							stack.pop();
							
							//Si la pila tiene mas de un elemento hay que meterlos uno a uno separados
							if(setTransitions.get(j)[4].length() > 1){
								String aux = setTransitions.get(j)[4];
								for(int k = 0; k < setTransitions.get(j)[4].length(); k++){
									String aux1 = aux.substring(aux.length()-1);
									aux = aux.substring(0,aux.length()-1);
									stack.push(aux1);
								}
							}else{//si no, no hace falta separar
								if(setTransitions.get(j)[4].equals("e")){
									//si es un, vac�o no hacer nada
									if(cadenaEntrada == "") {
										break;
									}else {
										System.out.println("1Entro"+cadenaEntrada);
										if(cadenaEntrada.equals("e")){
											System.out.println("2Entro"+cadenaEntrada);
											cadenaEntrada = "";
											System.out.println("Cadena "+cadenaEntrada);
											actualState = setF.get(0);
											cadenaEsAceptada(cadenaEntrada);
											break;
										}
									}
								}else{	
									stack.push(setTransitions.get(j)[4]);
								}
							}
							noTransiciones = true;
							break;//si se encuentra la transicion pasa al siguiente simbolo de la cadena
						}
						
					}else{
						cadenaEsAceptada(cadenaEntrada);
					}
				}
			} else{// si no hay transiciones, paramos
				break;
			}
		}//END FOR (NO QUEDAN TRANSICIONES)
		cadenaEsAceptada(cadenaEntrada);
	}
	
	/*
	 * COmMPRUEBA SI LA CADENA ES ACEPTADA POR EL AUT�MATA
	 */
	
	public void cadenaEsAceptada(String cadenaEntrada){
		if(setF.get(0).equals("e")){//autmata vaciado de pila
			System.out.println("Cadena de entrada "+cadenaEntrada.isEmpty()+" Pila "+stack.isEmpty());
			System.out.println("Cadena de entrada "+cadenaEntrada+" Pila "+stack);
			if(cadenaEntrada.isEmpty() && stack.isEmpty()){
				System.out.println("Cadena aceptada!");
			}else
				System.out.println("Cadena no aceptada!");
		}else{
			System.out.println("else Cadena de entrada "+cadenaEntrada.isEmpty()+" Pila "+stack.isEmpty());
			System.out.println("else Cadena de entrada "+cadenaEntrada+" Pila "+stack);
			for(int i = 0; i < setF.size(); i++){
				System.out.println("setF "+setF.get(i));
				if(cadenaEntrada.isEmpty() && actualState.equals(setF.get(i))){
					System.out.println("Cadena aceptada!");
					break;
				}else
					System.out.println("Cadena no aceptada");
			}
		}
	}

	
	/*
	 * Get and Set
	 */
	public String getSetQ() {
		String ret = null;
		for (int i = 0; i < setQ.size(); i++) {
			ret = ret+" "+setQ.get(i);
		}
		return ret;
	}

	public ArrayList<String> getSetSigma() {
		return setSigma;
	}

	public void setSetSigma(ArrayList<String> setSigma) {
		this.setSigma = setSigma;
	}

	public ArrayList<String> getSetR() {
		return setR;
	}

	public void setSetR(ArrayList<String> setR) {
		this.setR = setR;
	}

	public ArrayList<String> getSetF() {
		return setF;
	}

	public void setSetF(ArrayList<String> setF) {
		this.setF = setF;
	}

	public ArrayList<String[]> getSetTransitions() {
		return setTransitions;
	}

	public void setSetTransitions(ArrayList<String[]> setTransitions) {
		this.setTransitions = setTransitions;
	}

	public String getInitialState() {
		return initialState;
	}

	public void setInitialState(String initialState) {
		this.initialState = initialState;
	}

	public String getInitialPushdownSymbol() {
		return initialPushdownSymbol;
	}

	public void setInitialPushdownSymbol(String initialPushdownSymbol) {
		this.initialPushdownSymbol = initialPushdownSymbol;
	}

	public String getActualState() {
		return actualState;
	}

	public void setActualState(String actualState) {
		this.actualState = actualState;
	}

	public Stack<String> getStack() {
		return stack;
	}

	public void setStack(Stack<String> stack) {
		this.stack = stack;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
	
	
	
}
