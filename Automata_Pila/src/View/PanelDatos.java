package View;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class PanelDatos extends JPanel {

	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicación.
     */
    private PanelSimulacion principal;
	
    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------
    /**
     * Boton para la opción 1.
     */
    
    private JLabel setStates;
    private JLabel titleSetStates;
    private JLabel inictialState;
    private JLabel titleInictialState;
    private JLabel setAcceptanceState;
    private JLabel titleSetAcceptanceState;
    private JLabel setRibbonAlphabet;
    private JLabel titleSetRibbonAlphabet;
    private JLabel setStackAlphabet;
    private JLabel titleSetStackAlphabet;
    private JLabel stackBackgroundSymbol;
    private JLabel titleStackBackgroundSymbol;
    private JTextField chain;
    private JLabel titleChain;
    
    
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * @param pPrincipal Ventana principal. pPrincipal != null.
     * @return 
     */
    public PanelDatos( PanelSimulacion pPrincipal )
    {
        principal = pPrincipal;
     
        
        setBorder( new LineBorder(Color.GREEN));//COLOR BORDE
        TitledBorder tt = new TitledBorder( "Datos Automata" );
        tt.setTitleColor(Color.GREEN);
        setBorder(tt);// TITULO
        setBackground(Color.BLACK);
        setLayout( new GridLayout( 7,2  ) );// FILA , COLUMNAS
        
        
        // Estados
        titleSetStates = new JLabel("CONJUNTO DE ESTADOS");
        titleSetStates.setForeground(Color.WHITE);
        add( titleSetStates );
        
        setStates = new JLabel("Conjunto de estados");
        setStates.setForeground(Color.GREEN);
        add( setStates );
     
        // Estado inicial
        titleInictialState = new JLabel("ESTADO INICIAL");
        titleInictialState.setForeground(Color.WHITE);
        add( titleInictialState );
        
        inictialState = new JLabel("Estado inicial");
        inictialState.setForeground(Color.GREEN);
        add( inictialState );
        
        // Estados de aceptacion
        titleSetAcceptanceState = new JLabel("ESTADOS DE ACEPTACIÓN");
        titleSetAcceptanceState.setForeground(Color.WHITE);
        add( titleSetAcceptanceState );
        
        setAcceptanceState = new JLabel("Estado de aceptación");
        setAcceptanceState.setForeground(Color.GREEN);
        add( setAcceptanceState );
        
    // 	Alfabeto de la cinta
        titleSetRibbonAlphabet = new JLabel("ALFABETO DE LA CINTA");
        titleSetRibbonAlphabet.setForeground(Color.WHITE);
        add( titleSetRibbonAlphabet );
        
        setRibbonAlphabet = new JLabel("Alfabeto de la cinta");
        setRibbonAlphabet.setForeground(Color.GREEN);
        add( setRibbonAlphabet );
        
        // Alfabeto de la pila    
        titleSetStackAlphabet = new JLabel("ALFABETO DE LA PILA");
        titleSetStackAlphabet.setForeground(Color.WHITE);
        add( titleSetStackAlphabet );
        
        setStackAlphabet = new JLabel("Alfabeto de la pila");
        setStackAlphabet.setForeground(Color.GREEN);
        add( setStackAlphabet );
        
        // Simbolo del fondo de la pila
        titleStackBackgroundSymbol = new JLabel("FONDO DE LA PILA");
        titleStackBackgroundSymbol.setForeground(Color.WHITE);
        add( titleStackBackgroundSymbol );
        
        stackBackgroundSymbol = new JLabel("Simbolo del fondo de la pila");
        stackBackgroundSymbol.setForeground(Color.GREEN);
        add( stackBackgroundSymbol );
        
        // Cadena
        titleChain = new JLabel("CADENA");
        titleChain.setForeground(Color.WHITE);
        add( titleChain );
        
        chain = new JTextField ("abcbae");
        chain.setBackground(Color.BLACK);
        chain.setForeground(Color.WHITE);
        add( chain );

    }


	public PanelSimulacion getPrincipal() {
		return principal;
	}


	public void setPrincipal(PanelSimulacion principal) {
		this.principal = principal;
	}


	public JLabel getSetStates() {
		return setStates;
	}


	public void setSetStates(JLabel setStates) {
		this.setStates = setStates;
	}


	public JLabel getTitleSetStates() {
		return titleSetStates;
	}


	public void setTitleSetStates(JLabel titleSetStates) {
		this.titleSetStates = titleSetStates;
	}


	public JLabel getInictialState() {
		return inictialState;
	}


	public void setInictialState(JLabel inictialState) {
		this.inictialState = inictialState;
	}


	public JLabel getTitleInictialState() {
		return titleInictialState;
	}


	public void setTitleInictialState(JLabel titleInictialState) {
		this.titleInictialState = titleInictialState;
	}


	public JLabel getSetAcceptanceState() {
		return setAcceptanceState;
	}


	public void setSetAcceptanceState(JLabel setAcceptanceState) {
		this.setAcceptanceState = setAcceptanceState;
	}


	public JLabel getTitleSetAcceptanceState() {
		return titleSetAcceptanceState;
	}


	public void setTitleSetAcceptanceState(JLabel titleSetAcceptanceState) {
		this.titleSetAcceptanceState = titleSetAcceptanceState;
	}


	public JLabel getSetRibbonAlphabet() {
		return setRibbonAlphabet;
	}


	public void setSetRibbonAlphabet(JLabel setRibbonAlphabet) {
		this.setRibbonAlphabet = setRibbonAlphabet;
	}


	public JLabel getTitleSetRibbonAlphabet() {
		return titleSetRibbonAlphabet;
	}


	public void setTitleSetRibbonAlphabet(JLabel titleSetRibbonAlphabet) {
		this.titleSetRibbonAlphabet = titleSetRibbonAlphabet;
	}


	public JLabel getSetStackAlphabet() {
		return setStackAlphabet;
	}


	public void setSetStackAlphabet(JLabel setStackAlphabet) {
		this.setStackAlphabet = setStackAlphabet;
	}


	public JLabel getTitleSetStackAlphabet() {
		return titleSetStackAlphabet;
	}


	public void setTitleSetStackAlphabet(JLabel titleSetStackAlphabet) {
		this.titleSetStackAlphabet = titleSetStackAlphabet;
	}


	public JLabel getStackBackgroundSymbol() {
		return stackBackgroundSymbol;
	}


	public void setStackBackgroundSymbol(JLabel stackBackgroundSymbol) {
		this.stackBackgroundSymbol = stackBackgroundSymbol;
	}


	public JLabel getTitleStackBackgroundSymbol() {
		return titleStackBackgroundSymbol;
	}


	public void setTitleStackBackgroundSymbol(JLabel titleStackBackgroundSymbol) {
		this.titleStackBackgroundSymbol = titleStackBackgroundSymbol;
	}


	public JTextField getChain() {
		return chain;
	}


	public void setChain(JTextField chain) {
		this.chain = chain;
	}


	public JLabel getTitleChain() {
		return titleChain;
	}


	public void setTitleChain(JLabel titleChain) {
		this.titleChain = titleChain;
	}
    
    
	

}
