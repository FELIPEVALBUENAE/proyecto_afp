package View;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import Control.Connection;

/**
 * Panel de manejo de opciones.
 */
public class PanelSelect extends JPanel implements ActionListener {

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Comando para la opci�n 1.
     */
    private static final String SELECT = "SELECCIONAR";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicaci�n.
     */
    private PanelSalida principal;
    private Connection connection;

    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------

    /**
     * Bot�n para seleccionar fichero.
     */
    private JButton btnSelect;
    private JTextField tselect;
    private JFileChooser fc;
    
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * @param pPrincipal Ventana principal. pPrincipal != null.
     */
    public PanelSelect( PanelSalida pPrincipal )
    {
        principal = pPrincipal;
        connection = new Connection();

        //setBorder( new TitledBorder( "Opciones" ) );// TITULO
        setLayout( new BorderLayout() );		// FILA , COLUMNAS
        setBackground(Color.BLACK);

        // Bot�n reiniciar.
        btnSelect = new JButton( SELECT );
        btnSelect.setActionCommand( SELECT );
        btnSelect.setBackground(Color.CYAN);
        btnSelect.setBorder(new LineBorder(Color.CYAN));
        btnSelect.addActionListener( this );
        add( btnSelect, BorderLayout.EAST );
        
        // Select Automata
        tselect = new JTextField();
        tselect.setBackground(Color.BLACK);
        tselect.setForeground(Color.WHITE);
        add( tselect, BorderLayout.CENTER );

    }

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JFileChooser fc = new JFileChooser(); //Creamos el objeto JFileChooser
		fc.showOpenDialog(this);
		fc.setMultiSelectionEnabled(false); //Indicamos que podemos seleccionar varios ficheros
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); //Indicamos lo que podemos seleccionar
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.TXT", "txt"); //Creamos el filtro
		fc.setFileFilter(filtro); //Le indicamos el filtro
		File file = fc.getSelectedFile();
		if(file != null) {
			tselect.setText(file.getAbsolutePath());
			System.out.println("Path "+file.getAbsolutePath());
			connection.setPathFile(file.getAbsolutePath());
		}
			
	}
	
	/*
	 * Get and Set
	 */

	public PanelSalida getPrincipal() {
		return principal;
	}

	public void setPrincipal(PanelSalida principal) {
		this.principal = principal;
	}

	public JButton getBtnSelect() {
		return btnSelect;
	}

	public void setBtnSelect(JButton btnSelect) {
		this.btnSelect = btnSelect;
	}

	public JTextField getTselect() {
		return tselect;
	}

	public void setTselect(JTextField tselect) {
		this.tselect = tselect;
	}

	public JFileChooser getFc() {
		return fc;
	}

	public void setFc(JFileChooser fc) {
		this.fc = fc;
	}

	public static String getSelect() {
		return SELECT;
	}
	
	

}

