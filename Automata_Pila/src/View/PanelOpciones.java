package View;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 * Panel de manejo de opciones.
 */
public class PanelOpciones extends JPanel implements ActionListener
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Comando para la opci�n 1.
     */
    private static final String EJECUTAR = "EJECUTAR";

    /**
     * Comando para la opci�n 2.
     */
    private static final String REINICIAR = "REINICIAR";

    /**
     * Comando para reiniciar valores.
     */
    private static final String CERRAR = "CERRAR";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicaci�n.
     */
    private Ventana principal;

    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------

    /**
     * Bot�n para la ejecutar.
     */
    private JButton btnEjecutar;

    /**
     * Bot�n para la reiniciar.
     */
    private JButton btnReiniciar;

    /**
     * Bot�n para cerrar.
     */
    private JButton btnCerrar;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * @param pPrincipal Ventana principal. pPrincipal != null.
     */
    public PanelOpciones( Ventana pPrincipal )
    {
        principal = pPrincipal;

        //setBorder( new TitledBorder( "Opciones" ) );// TITULO
        setLayout( new GridLayout( 1, 3 ) );		// FILA , COLUMNAS
        setBackground(Color.BLACK);

        // Bot�n reiniciar.
        btnEjecutar = new JButton( EJECUTAR );
        btnEjecutar.setActionCommand( EJECUTAR );
        btnEjecutar.setBackground(Color.green);
        btnEjecutar.setBorder(new LineBorder(Color.green));
        btnEjecutar.addActionListener( this );
        add( btnEjecutar );

        // Bot�n opci�n 1.
        btnReiniciar = new JButton( REINICIAR );
        btnReiniciar.setActionCommand( REINICIAR );
        btnReiniciar.setBackground(Color.yellow);
        btnReiniciar.setBorder(new LineBorder(Color.yellow));
        btnReiniciar.addActionListener( this );
        add( btnReiniciar );

        // Bot�n opci�n 2.
        btnCerrar = new JButton( CERRAR );
        btnCerrar.setActionCommand( CERRAR );
        btnCerrar.setBackground(Color.red);
        btnCerrar.setBorder(new LineBorder(Color.red));
        btnCerrar.addActionListener( this );
        add( btnCerrar );
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Manejo de los eventos de los botones. <br>
     * @param pEvento Acci�n que gener� el evento.
     */
    public void actionPerformed( ActionEvent pEvento )
    {
        if( EJECUTAR.equals( pEvento.getActionCommand( ) ) )
        {
        	try {
				principal.reqIncio();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        else if( REINICIAR.equals( pEvento.getActionCommand( ) ) )
        {
        }
        else if( CERRAR.equals( pEvento.getActionCommand( ) ) )
        {
        	principal.dispose();
        }
    }
    
    

}

