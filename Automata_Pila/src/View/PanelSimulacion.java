package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;

public class PanelSimulacion extends JPanel{

	
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicación.
     */
    private Ventana principal;
	
    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------
    
    private PanelDatos panelDatosAu;
    private PanelSalida panelSalida;
    
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * @param pPrincipal Ventana principal. pPrincipal != null.
     * @return 
     */
    public PanelSimulacion( Ventana pPrincipal )
    {
        principal = pPrincipal;
     
        
        /*setBorder( new LineBorder(Color.BLACK));//COLOR BORDE
        setBorder( new TitledBorder( "Harina" ));// TITULO*/
        
        setBackground(Color.black);
        setLayout( new GridLayout( 1,2  ) );// FILA , COLUMNAS
        
        panelDatosAu = new PanelDatos(this);
		add( panelDatosAu);
		
		panelSalida = new PanelSalida(this);
		add( panelSalida);
        
     setBackground(Color.WHITE);
        
    }

    /*
     * Get and Set
     */
	public Ventana getPrincipal() {
		return principal;
	}

	public void setPrincipal(Ventana principal) {
		this.principal = principal;
	}

	public PanelDatos getPanelDatosAu() {
		return panelDatosAu;
	}

	public void setPanelDatosAu(PanelDatos panelDatosAu) {
		this.panelDatosAu = panelDatosAu;
	}

	public PanelSalida getPanelSalida() {
		return panelSalida;
	}

	public void setPanelSalida(PanelSalida panelSalida) {
		this.panelSalida = panelSalida;
	}
	
    

}
