package View;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Control.Connection;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;

public class Ventana extends JFrame  {
	
	private PanelOpciones panelOpciones;
	private PanelImagen panelImagen;
	private PanelSimulacion panelSimulacion;
	private Connection connection;
	
	
	
	public Ventana(){
		super();
		inicializar();
		inicializarComponentes();
		
		
	}
	
	public void inicializar(){
		
		this.setTitle("Automata de Pila"); // titulo de la ventana
		this.setSize(800, 720);            // dimension de la ventana 1200 720
		this.setLocationRelativeTo(null);  // centrar la ventana en la pantalla
		this.setResizable(false);          // no permitir que la ventana sea redimensionada
		this.setLayout(new BorderLayout()); 			   // sin layout para dar posiciones a los componentes 	
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);  // termina el proceso cuando se cierra la ventana
		this.setBackground(Color.black);
		this.setUndecorated(true);
		
	}
	
	public void inicializarComponentes(){
		
		panelImagen = new PanelImagen();
		add(panelImagen, BorderLayout.NORTH);
		
		panelOpciones = new PanelOpciones( this );
	        add( panelOpciones, BorderLayout.SOUTH );
		
	    /*panelSabor = new PanelSabor(this);
	    	add( panelSabor, BorderLayout.WEST );
	    	
	    panelHarina = new PanelHarina(this);
		    add( panelHarina, BorderLayout.EAST);*/
	    
		panelSimulacion = new PanelSimulacion (this);
			add( panelSimulacion, BorderLayout.CENTER);
			

	}
	
	// -----------------------------------------------------------------
    // Puntos de Extensi�n
    // -----------------------------------------------------------------

    /**
     * M�todo para la extensi�n 1.
     * @throws IOException 
     */
    public void reqIncio( ) throws IOException
    {
        connection.datosIniciales();
    }

    /**
     * M�todo para la extensi�n 2.
     */
    public void reqFuncOpcion2( )
    {
        String resultado = "Respuesta 2";
        JOptionPane.showMessageDialog( this, resultado, "Respuesta", JOptionPane.INFORMATION_MESSAGE );
    }
    
    /**
     * M�todo para la extensi�n 3.
     */
    public void reiniciar( )
    {
        String resultado = "Metodo para reiniciar proceso";
        JOptionPane.showMessageDialog( this, resultado, "Respuesta", JOptionPane.INFORMATION_MESSAGE );
    }
    
    /*
     * Get and Set
     */

	public PanelOpciones getPanelOpciones() {
		return panelOpciones;
	}

	public void setPanelOpciones(PanelOpciones panelOpciones) {
		this.panelOpciones = panelOpciones;
	}

	public PanelImagen getPanelImagen() {
		return panelImagen;
	}

	public void setPanelImagen(PanelImagen panelImagen) {
		this.panelImagen = panelImagen;
	}

	public PanelSimulacion getPanelSimulacion() {
		return panelSimulacion;
	}

	public void setPanelSimulacion(PanelSimulacion panelSimulacion) {
		this.panelSimulacion = panelSimulacion;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	
	
	

}
