package View;

import java.io.IOException;

import Model.Automata;

public class Main {
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		 Automata automata = new Automata(); 
		 automata.mostrarInformacionAutomata();
		 automata.ejecutarAutomata();
	}
}
