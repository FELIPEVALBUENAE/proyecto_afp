package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class PanelSalida extends JPanel {

	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Ventana principal de la aplicaci�n.
     */
    private PanelSimulacion principal;
	
    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------
    /**
     * Boton para la opci�n 1.
     */
    
    private JTextArea consol;
    private JLabel titleConsol;
    private JScrollPane scrollpane;
    private PanelSelect panelSelect;
    
    
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Constructor del panel.<br>
     * @param pPrincipal Ventana principal. pPrincipal != null.
     * @return 
     */
    public PanelSalida( PanelSimulacion pPrincipal )
    {
        principal = pPrincipal;
     
        
        setBorder( new LineBorder(Color.GREEN));//COLOR BORDE
        TitledBorder tt = new TitledBorder( "Computos" );
        tt.setTitleColor(Color.GREEN);
        setBorder(tt);// TITULO
        setBackground(Color.BLACK);
        setLayout( new BorderLayout());// FILA , COLUMNAS
        
        
        
        // Cadena
        titleConsol = new JLabel("ESTADO: --");
        titleConsol.setFont(new Font("Serif", Font.BOLD, 20));
        titleConsol.setForeground(Color.WHITE);
        add(titleConsol, BorderLayout.NORTH );
        
        consol = new JTextArea("abcbae");
        scrollpane = new JScrollPane(consol);
        scrollpane.setBounds(10,50,400,300);
        consol.setBackground(Color.BLACK);
        consol.setForeground(Color.WHITE);
        consol.setBorder(new LineBorder(Color.WHITE));
        add( consol, BorderLayout.CENTER );
        
        panelSelect = new PanelSelect(this);
		add(panelSelect, BorderLayout.SOUTH);

    }

    /*
     * Get And Set
     */

	public PanelSimulacion getPrincipal() {
		return principal;
	}


	public void setPrincipal(PanelSimulacion principal) {
		this.principal = principal;
	}


	public JTextArea getConsol() {
		return consol;
	}


	public void setConsol(JTextArea consol) {
		this.consol = consol;
	}


	public JLabel getTitleConsol() {
		return titleConsol;
	}


	public void setTitleConsol(JLabel titleConsol) {
		this.titleConsol = titleConsol;
	}


	public JScrollPane getScrollpane() {
		return scrollpane;
	}


	public void setScrollpane(JScrollPane scrollpane) {
		this.scrollpane = scrollpane;
	}


	public PanelSelect getPanelSelect() {
		return panelSelect;
	}


	public void setPanelSelect(PanelSelect panelSelect) {
		this.panelSelect = panelSelect;
	}
    
     
	

}
